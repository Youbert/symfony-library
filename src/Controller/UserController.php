<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class UserController
 * @package App\Controller
 * @Route("user")
 */
class UserController extends AbstractController
{
    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/")
     */
    public function index(Request $request, EntityManagerInterface $em, UserRepository $userRepository)
    {
        $users = $userRepository->findAll();

        return $this->render('user/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     * @Route("/edit/{id}", defaults={"id":null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id)
    {
        if (is_null($id)) {//création
            $user = new User();
        } else {//modification
            $user = $em->find(User::class, $id);

            //404 si l'id n'est pas en BDD
            if (is_null($user)) {
                throw new NotFoundHttpException();
            }
        }

        // création du formulaire relié à la catégorie
        $form = $this->createForm(UserType::class, $user);

        // le formulaire analyse la requête et fait le mapping
        // avec l'entité s'il a été soumis
        $form->handleRequest($request);

        dump($user);

        // si le formulaire est envoyé / a été soumis
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //enregistrement en bdd
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'La catégorie est enregistrée.');

                //redirection vers la liste
                return $this->redirectToRoute('app_user_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }


        return $this->render('/user/edit.html.twig',
            [
                // passage du formulaire au template
                'form' => $form->createView()
            ]
        );

    }

    /**
     * @param EntityManagerInterface $em
     * @param User $user
     * @route ("/delete/{id}")
     */
    public function delete(EntityManagerInterface $em, User $user)
    {
        $em->remove($user);
        $em->flush();
        $this->addFlash(
            'success',
            'Le commentaire est bien supprimé'
        );
        return $this->redirectToRoute(
            'app_user_index'
        );
    }

}
