<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Form\SearchBookType;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BookController
 * @package App\Controller
 * @Route("book")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/")
     * @param Request $request
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request,EntityManagerInterface $em, BookRepository $bookRepository)
    {
        //$books = $bookRepository->findAll();

        $searchForm = $this->createForm(SearchBookType::class);

        $searchForm->handleRequest($request);

        $books= $bookRepository->search((array)$searchForm->getData());

        return $this->render('book/index.html.twig', [
                'books' => $books,
                'search_form' => $searchForm->createView()
            ]
        );
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/edit/{id}", defaults={"id":null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id
    ){
        if (is_null($id)){//création
            $book = new Book();
        } else {//modification
            $book = $em->find(Book::class, $id);

            //404 si l'id n'est pas en BDD
            if (is_null($book)){
                throw new NotFoundHttpException();
            }
        }

        // création du formulaire relié à la catégorie
        $form = $this->createForm(BookType::class, $book);

        // le formulaire analyse la requête et fait le mapping
        // avec l'entité s'il a été soumis
        $form->handleRequest($request);

        dump($book);

        // si le formulaire est envoyé / a été soumis
        if($form->isSubmitted()){
            if($form->isValid()){

                //enregistrement en bdd
                $em->persist($book);
                $em->flush();

                $this->addFlash('success', 'La catégorie est enregistrée.');

                //redirection vers la liste
                return $this->redirectToRoute('app_book_index');
            }else{
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render('/book/edit.html.twig',
            [
                // passage du formulaire au template
                'form' => $form->createView()
            ]
        );
    }


    /**
     * @param EntityManagerInterface $em
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        Book $book
    ){

        $em->remove($book);
        $em->flush();
        $this->addFlash(
            'success',
            'Le commentaire est bien supprimé'
        );
        return $this->redirectToRoute(
            'app_book_index'
        );
    }

}
