<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Form\LoanType;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LoanController
 * @package App\Controller
 * @Route("loan")
 */
class LoanController extends AbstractController
{
    /**
     * @Route("/")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param LoanRepository $loanRepository
     */
    public function index(Request $request, EntityManagerInterface $em, LoanRepository $loanRepository)
    {
        $loans = $loanRepository->findAll();

        return $this->render('loan/index.html.twig', [
            'loans' => $loans,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     * @Route("/edit/{id}", defaults={"id":null}, requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $em, $id
    )
    {
        if (is_null($id)) {//création
            $loan = new Loan();
        } else {//modification
            $loan = $em->find(Loan::class, $id);

            //404 si l'id n'est pas en BDD
            if (is_null($loan)) {
                throw new NotFoundHttpException();
            }
        }

        // création du formulaire relié à la catégorie
        $form = $this->createForm(LoanType::class, $loan);

        // le formulaire analyse la requête et fait le mapping
        // avec l'entité s'il a été soumis
        $form->handleRequest($request);

        dump($loan);

        // si le formulaire est envoyé / a été soumis
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                //enregistrement en bdd
                $em->persist($loan);
                $em->flush();

                $this->addFlash('success', "L'emprunt est enregistré.");

                //redirection vers la liste
                return $this->redirectToRoute('app_loan_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render('/loan/edit.html.twig',
            [
                // passage du formulaire au template
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/delete/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        Loan $loan
    )
    {


        $em->remove($loan);
        $em->flush();
        $this->addFlash(
            'success',
            'Le commentaire est bien supprimé'
        );
        return $this->redirectToRoute(
            'app_loan_index'
        );
    }


    /**
     * @Route("/duedate/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function dueDate(EntityManagerInterface $em, Loan $loan)
    {
       $presentDate = $loan->getEndDate();

       if(is_null($presentDate)){
           $presentDate = new \DateTime();
           $loan->setEndDate($presentDate);

           $em->persist($loan);
           $em->flush();


       }
        return $this->redirectToRoute('app_loan_index');
    }
}

