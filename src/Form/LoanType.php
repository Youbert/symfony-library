<?php

namespace App\Form;

use App\Entity\Book;
use App\Entity\Loan;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('startDate',
              //  DateType::class,
                //[
                  // 'label' => 'Date de début',
                  // 'required' => true,
                  // 'widget' => 'single_text',
                  //  'format' => 'dd/MM/yyyy',
                  //  'html5' => false
               // ]
           // )
           // ->add('endDate',
             //   DateType::class,
              //  [
              //      'label' => 'Date de fin',
               //     'required' => true,
                //    'widget' => 'single_text',
                //    'format' => 'dd/MM/yyyy',
                  //  'html5' => false
              //  ])
            ->add('book',
                EntityType::class,
                [
                    'class' => Book::class,

                    'label' => 'titre',
                    'placeholder' => 'Choisissez un livre',
                    'required' => true
                ]
            )
            ->add('user',
                EntityType::class,
                [
                    'class' => User::class,
                    'label' => 'User',
                    'placeholder' => 'Choisissez un user',
                    'required' => true
                ]
                )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Loan::class,
        ]);
    }
}
